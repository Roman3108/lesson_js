
var userNum = parseInt(prompt("Вгадайте число від 0 до 10"), 0);
var userTimeNum = parseInt(prompt("Введіть будь яку хвилину в одній годині"));
var userSeasonNum = parseInt(prompt("Введіть число пори року від 1 до 4"));


document.write("<div class='container'>");
    document.write("<div class='header'>");
        if (userNum < 0) {
            document.write("<div class = 'header num'>Не попали в діапазон</div>");
        } else if (userNum == 10) {
            document.write("<div class = 'header numTrue'>Вірно</div>");
        } else if (userNum > 10) {
            document.write("<div class = 'header num'>Не попали в діапазон</div>");
        } else {
            document.write("<div class = 'header numFalse'>Невірно</div>");
        }
        
    document.write("</div>");

    document.write("<div class='main'>");
        if (userTimeNum < 0) {
            document.write("<div class = 'main time'>В годині хвилини від 0 до 59</div>");
        } else if (userTimeNum < 16) {
            document.write("<div class = 'main time0-15'>Зараз перша чверть години</div>");
        } else if(userTimeNum < 31) {
            document.write("<div class = 'main time16-30'>Зараз друга чверть години</div>");
        } else if(userTimeNum < 46) {
            document.write("<div class = 'main time31-45'>Зараз третя чверть години</div>");
        } else if(userTimeNum < 60) {
            document.write("<div class = 'main time46-59'>Зараз четверта чверть години</div>");
        } else if (userTimeNum > 59) {
            document.write("<div class = 'main time'>В годині хвилини від 0 до 59</div>");
        };
            
    document.write("</div>");
    document.write("<div class='footer'>");
        if (userSeasonNum < 0) {
            document.write("<div class = 'footer seasons'>В році чотири пори</div>");
        } else if (userSeasonNum == 1) {
            document.write("<div class = 'footer winter'>Зараз пора року \"Зима\"</div>");
        } else if(userSeasonNum == 2) {
            document.write("<div class = 'footer spring'>Зараз пора року \"Весна\"</div>");
        } else if(userSeasonNum == 3) {
            document.write("<div class = 'footer summer'>Зараз пора року \"Літо\"</div>");
        } else if(userSeasonNum == 4) {
            document.write("<div class = 'footer outemn'>Зараз пора року \"Осінь\"</div>");
        } else if (userSeasonNum > 4) {
            document.write("<div class = 'footer seasons'>В році чотири пори</div>");
        };
    document.write("</div>");
document.write("</div>");

