

const wallet = {
    userName : "Roman",
    bitcoin : {
        logo : "<img src='https://d235dzzkn2ryki.cloudfront.net/bitcoin_normal.png' alt='bitcoin'/>",
        price : 39200.00,
        coins : 5,
    },
    
    ethereum : {
        logo : "<img src='https://d235dzzkn2ryki.cloudfront.net/ethereum_large.png' alt='ethereum'/>",
        price : 2886.30,
        coins : 12,
    },
   
    triall : {
        logo : "<img src='https://d235dzzkn2ryki.cloudfront.net/triall_large.png' alt='triall'/>",
        price : 0.039777,
        coins : 1200,
    },

    show : function() {
        document.write("Добрий день " + wallet.userName + "!<br>")
        document.write(wallet.bitcoin.logo + " ціна - $" + wallet.bitcoin.price + ", у Вас на рахунку " + wallet.bitcoin.coins + " монет" + "<br>")
        document.write(wallet.ethereum.logo + " ціна - $" + wallet.ethereum.price + ", у Вас на рахунку " + wallet.ethereum.coins + " монет" + "<br>")
        document.write(wallet.triall.logo + " ціна - $" + wallet.triall.price + ", у Вас на рахунку " + wallet.triall.coins + " монет" + "<br>")
    },
}
wallet.show();